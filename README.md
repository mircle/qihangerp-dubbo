# 启航电商ERP系统


启航电商ERP系统是一套简单、实用、覆盖全流程的电商系统，本项目采用后端采用Dubbo3微服务架构开发，前端采用Vue+Element开发。 

**老版本请移步：**

[启航电商ERP系统](https://gitee.com/qiliping/qihang-erp)


系统支持供应商一件代发和仓库发货两种发货方式，功能覆盖采购管理、订单处理、网店订单同步、发货管理、售后处理、网店售后同步、商品管理、网店商品同步关联、仓库出入库管理等功能，基本上覆盖了电商日常电商业务。 

支持主流电商平台接口对接，包括：淘宝、京东、拼多多、抖店、视频号小店、快手、小红书。

![预览](docs/preview.png)


## 项目介绍
**启航电商ERP可以说是我这五年以来的工作经验成果。**

公司从2019年踏入电商以来，一直都是由我组建和带领一帮技术人员从零开始建设了一套完全适应公司业务需要的电商ERP系统，包括WMS仓库系统、OMS订单处理系统、财务系统、直播运营系统等子系统组成。主要功能模块包括：采购模块、出入库模块、订单发货模块、网店订单管理模块、电子面单打印模块。公司ERP对接了批批网、1688、蘑菇街、淘宝天猫、拼多多、抖店、快手小店平台。

### 公司应用场景一：抖店直播


```mermaid
graph TD
A[早上7点开播] -->B(10点结束直播-一般是500单左右)
    B --> C[使用ERP系统打单-对接平台电子面单接口]
    C --> E(库存核对)
    E --> F[库存不足]
    F --> F1(采购补货)
    F1 --> G1
    E --> G(库存充足)
    G --> G1[打印订单]
    G1 --> G5(推送快递单号到平台)
    G1 --> G2(生成备货单)
    G2 --> G3(仓库人员备货出库)
    G3 --> H(完成)
    G5 --> H
```


## 一、功能模块
### 供应链管理
+ 供应商管理：管理供应商信息
+ 采购订单管理：管理采购流程，包括供应商选择、采购订单生成、采购合同管理等。
+ 采购账单管理
+ 采购退货管理
+ 采购物流管理：跟踪采购订单物流信息。
+ 供应商代发管理：管理一件代发订单。
+ 代发账单管理

**采购流程**

```mermaid
graph LR
A[创建采购订单] -->B(审核)
    B --> C[供应商确认]
    C --> E[供应商发货]
    E --> F1(生成物流信息)
    F1 --> G1[确认收货]
    G1 --> G3[生成入库单]
    G3 --> G4(入库)
    G1 --> G2[生成财务应付及明细]
    G4 --> H(完成)
    G2 --> H

```

### 订单管理
+ 创建订单：手动创建订单。
+ 店铺订单管理：处理和管理多平台订单的流程，包括订单录入、处理、发货等。
  + 支持淘宝、拼多多、抖店、快手小店、小红书平台订单接口；
  + 支持淘宝订单excel导入；
  + 支持手动添加订单；
  + 订单确认到仓库；
+ 订单查询：查询所有订单信息。
+ 店铺管理：管理店铺信息、店铺商品上下架信息等。

**订单处理流程**
```mermaid
graph LR
A[录入订单] -->B(确认订单)
    B --> C[供应商代发]
    B --> D[仓库发货]
    C --> E[供应商发货]
    E --> F1(生成应付)
    D --> H
    F1 --> H(完成)


```
### 发货管理
+ 订单备货：生成拣货单；
+ 拣货出库：拣货出库、生成出库单减库存；
+ 打包发货：记录包裹信息、物流发货、同步发货状态；
+ 物流跟踪：跟踪发货快递物流；
+ 供应商代发管理：管理供应商代发的订单

**发货流程**
```mermaid
graph TD
A[查询备货清单] -->B(生成拣货单)
    B --> C[拣货出库]
    C --> E(减库存)
    E --> F[打包发货]
    F --> F1(记录包裹信息)
    F1 --> G(填写物流信息)
    G --> G1[同步发货状态]
    G1 --> H(完成)
    G --> G2[生成订单应收款项]
    G2 --> H

```

### 售后管理
对退货、换货、维修等售后处理进行管理，包括退款审核、退货入库、退款处理等环节。
+ 店铺售后管理：处理和管理多平台售后包括录入售后数据、退货入库、换货处理等。
  + 支持拼多多、抖店、快手小店、小红书平台售后接口；
  + 支持手动录入、备注；
+ 退货处理：数据录入、仓库收货确认、库存处理等。
+ 换货处理：数据录入、仓库收货确认、仓库发货、库存处理等。

**退款退货流程**
```mermaid
graph LR
A[录入退款退货] -->B(仅退款)
    B --> C[生成销售应付]
    A --> D(退货退款)
    D --> E[仓库收货]
    E --> F1(生成销售应付)
    E --> F[退货入库处理]
    C --> H
    F --> H
    F1 --> H(完成)
```

### 店铺管理
+ 淘宝商品管理：同步淘宝店铺商品，关联到ERP商品（用于仓库发货处理）；
+ 多多商品管理：同步多多店铺商品，关联到ERP商品（用于仓库发货处理）；
+ 抖店商品管理：同步抖店店铺商品，关联到ERP商品（用于仓库发货处理）；
+ 店铺设置：网店管理、API参数设置；

### 库存管理

+ 入库管理
+ 出库管理
+ 库存查询：跟踪和管理库存，包括批次管理、库存盘点、库存调整、库存预警等。
+ 库位管理


### 商品管理
商品信息、分类信息、属性信息等管理。


## 二、项目说明
### 2.1 主要版本
+ 后端：
  + `Java`: 17
  + `SpringBoot`: 3.0.13
  + `Dubbo`：3.2.11
+ 前端：
  + `vue2`
  + `elementUI`

### 2.2 存储及中间件
+ `MySQL`:数据库,版本8.x。
+ `minio文`:文件存储,用于图片存储。
+ `Redis`:在线用户信息、缓存。
+ `Nacos`:版本2.2.0以上，配置中心、注册中心。

### 2.3 项目结构
#### 2.3.1 api
后端接口服务，端口8080，采用dubbo调用其他微服务。

#### 2.3.2 interfaces
微服务接口类库（包括domain、service-interface）

#### 2.3.3 service
微服务实现层，service-impl

#### 2.3.4 common
公共类库

#### 2.3.5 vue
Vue前端项目

## 三、如何使用？
### 3.1、开发环境配置
+ MySQL数据库创建
  + 运行MySQL脚本`docs\sql\qihang-erp.sql`导入数据到主库`qihang-erp`

  + 运行MySQL脚本`docs\sql\nacos.sql`导入数据到nacos库`nacos`


+ 启动nacos
  + 修改Nacos数据库配置 `nacos\conf\application.properties`
  ```
  db.url.0=jdbc:mysql://127.0.0.1:3306/nacos?characterEncoding=utf8&connectTimeout=1000&socketTimeout=3000&autoReconnect=true&useUnicode=true&useSSL=false&serverTimezone=Asia/Shanghai
  db.user.0=root
  db.password.0=Andy_123
  ```

  + 启动Nacos
  + 添加Nacos配置
    + 旧版项目配置名`ecerp-dev`（配置内容从docs\ecerp-dev.yaml复制即可）
    + 新版项目配置名`qihangec-erp.yaml`（配置内容从docs\ecerp-dev.yaml复制即可）
+ 启动Redis

  
+ 启动minio


  
### 3.2、启动后端

+ 启动`service`下面的所有微服务
+ 启动`api`项目

### 3.3、启动前端 `vue`
+ `npm install`
+ `npm run dev`
+ 打包`npm run build:prod`
+ 访问web
  + 访问地址：`http://localhost`
  + 登录名：`admin`
  + 登录密码：`admin123`

## 四、获取一键演示包

启航电商ERP系统自从开源以来，收到很多兄弟们的关注，也很多兄弟们想看演示效果，由于项目是非商业化的没有财力去支撑演示环境服务器，为了满足兄弟们想看系统演示，作者特地制作了一个一键演示包，获取之后直接在Windows电脑中即可运行。

[一键演示包获取地址](https://mp.weixin.qq.com/s?__biz=MjM5MTM1ODg0Mg==&mid=2447551448&idx=1&sn=53aab9a0123caf912cc89accb5fb226d&chksm=b2a0c1cd85d748dbde1eb7d511e3933d0ebd9df7d4e8f35e4d3eba7624ab5264f10900845b11&payreadticket=HKl0eiGUReMt0NYHRj2hUZS-ANiV_oN0p8XIfKxeIqVs2ZdYE1Rts-9YlN7uXwEWnLcq2H4#rd)


## 五、支持作者

**感谢大家的关注与支持！希望利用本人从事电商10余年的经验帮助到大家提升工作效率！**

💖 如果觉得有用记得点 Star⭐


### 1、有偿服务
+ 提供部署服务
+ 提供演示包服务
+ 提供定制化开发服务
+ 提供系统培训服务
+ 提供版本商业化支持服务
+ 提供电商系统软著代申请服务（文档、源代码）
+ 提供电商平台appkey申请协助服务

### 2、更多服务

更多服务，请关注作者微信公众号：qihangerp168

<img src="docs/qihangerp168.jpg" width="300px" />


💖 欢迎一起交流！

### 3、捐助支持
作者为兼职做开源,平时还需要工作,如果帮到了您可以请作者吃个盒饭

<img src="docs/weixinzhifu.jpg" width="300px" />
<img src="docs/zhifubao.jpg" width="300px" />




